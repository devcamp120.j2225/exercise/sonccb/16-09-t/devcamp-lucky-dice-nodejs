//import thư viện mongoose
const mongoose = require("mongoose");


//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const prizeHistorySchema = new Schema({
 user:[
  {
   type:mongoose.Types.ObjectId,
   ref:"User",
   required:true
  }
 ],
 prize:[
  {
   type:mongoose.Types.ObjectId,
   ref:"Prize",
   required:true
  }
 ]
},{
 timestamps:true
}
)
module.exports = mongoose.model("PrizeHistory",prizeHistorySchema);