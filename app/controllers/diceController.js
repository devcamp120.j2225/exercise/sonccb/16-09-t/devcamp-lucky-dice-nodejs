// Import thư viện mongoose
const mongoose = require('mongoose');

// Import models
const diceHistoryModel = require('../models/diceHistoryModel');
const prizeModel = require('../models/prizeModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');

// Create new dice
const diceController = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let username = req.body.username;
     let firstname = req.body.firstname;
     let lastname = req.body.lastname;

     let dice = Math.floor(Math.random() * 6) + 1;
     // B2: Validate dữ liệu
     if (!username) {
          return res.status(400).json({
               message: 'Username is required!'
          })
     }
     if (!firstname) {
          return res.status(400).json({
               message: 'Firstname is required!'
          })
     }
     if (!lastname) {
          return res.status(400).json({
               message: 'Lastname is required!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     // Sử dụng userModel tìm kiếm bằng username
     userModel.findOne({ username: username }, (error, userExist) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          } else {
               if (!userExist) {
                    // Nếu user ko tồn tại trong hệ thống, tạo user mới
                    let newUser = {
                         _id: mongoose.Types.ObjectId(),
                         username: username,
                         firstname: firstname,
                         lastname: lastname
                    }
                    userModel.create(newUser, (error, userCreated) => {
                         if (error) {
                              return res.status(500).json({
                                   message: error.message
                              })
                         } else {
                              // Tung xúc xắc 1 lần, lưu vào dice history
                              let newDiceHistory = {
                                   _id: mongoose.Types.ObjectId(),
                                   user: userCreated._id,
                                   dice: dice
                              }
                              diceHistoryModel.create(newDiceHistory, (error, diceHistoryCreated) => {
                                   if (error) {
                                        return res.status(500).json({
                                             message: error.message
                                        })
                                   } else {
                                        if (dice < 3) {
                                             // Nếu nhỏ hơn 3, ko đc voucher và prize
                                             return res.status(200).json({
                                                  dice: dice,
                                                  prize: null,
                                                  voucher: null
                                             })
                                        } else {
                                             // Nếu lớn hơn 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                                             // Get the count of all vouchers
                                             voucherModel.count().exec((error, countVoucher) => {
                                                  // Get a random entry
                                                  let randomVoucher = Math.floor(Math.random * countVoucher);
                                                  // Again query all vouchers, but only fetch one offset by our random
                                                  voucherModel.findOne()
                                                       .skip(randomVoucher)
                                                       .exec((error, findVoucher) => {
                                                            let newVoucherHistory = {
                                                                 _id: mongoose.Types.ObjectId(),
                                                                 user: userCreated._id,
                                                                 voucher: findVoucher._id
                                                            }
                                                            // Lưu vào voucher history
                                                            voucherHistoryModel.create(newVoucherHistory, (error, voucherHistoryCreated) => {
                                                                 if (error) {
                                                                      return res.status(500).json({
                                                                           message: error.message
                                                                      })
                                                                 } else {
                                                                      if (error) {
                                                                           return res.status(500).json({
                                                                                message: error.message
                                                                           })
                                                                      } else {
                                                                           // User mới ko có prize
                                                                           return res.status(200).json({
                                                                                dice: dice,
                                                                                prize: null,
                                                                                voucher: findVoucher
                                                                           })
                                                                      }
                                                                 }
                                                            })
                                                       })
                                             })
                                        }
                                   }
                              })
                         }
                    })
               } else {
                    // Nếu user đã tồn tại trong hệ thống
                    // Tung xúc xắc 1 lần, lưu vào dice history
                    let newDiceHistory = {
                         _id: mongoose.Types.ObjectId(),
                         user: userExist._id,
                         dice: dice
                    }
                    diceHistoryModel.create(newDiceHistory, (error, diceHistoryCreate) => {
                         if (error) {
                              return res.status(500).json({
                                   message: error.message
                              })
                         } else {
                              if (dice < 3) {
                                   // Nếu nhỏ hơn 3, ko đc voucher và prize
                                   return res.status(200).json({
                                        dice: dice,
                                        prize: null,
                                        voucher: null
                                   })
                              } else {
                                   // Nếu lớn hơn 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                                   voucherModel.count().exec((error, countVoucher) => {
                                        let randomVoucher = Math.floor(Math.random * countVoucher);
                                        voucherModel.findOne().skip(randomVoucher).exec((error, findVoucher) => {
                                             // Lưu vào voucher history
                                             let newVoucherHistory = {
                                                  _id: mongoose.Types.ObjectId(),
                                                  user: userExist._id,
                                                  voucher: findVoucher._id
                                             }
                                             voucherHistoryModel.create(newVoucherHistory, (error, voucherHistoryCreated) => {
                                                  if (error) {
                                                       return res.status(500).json({
                                                            message: error.message
                                                       })
                                                  } else {
                                                       // Lấy 3 lần gieo xúc xắc gần nhất của user
                                                       diceHistoryModel.find()
                                                            .sort({ _id: -1 })
                                                            .limit(3)
                                                            .exec((error, last3DiceHistory) => {
                                                                 if (error) {
                                                                      return res.status(500).json({
                                                                           message: error.message
                                                                      })
                                                                 } else {
                                                                      // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                                      if (last3DiceHistory.length < 3) {
                                                                           return res.status(200).json({
                                                                                dice: dice,
                                                                                prize: null,
                                                                                voucher: findVoucher
                                                                           })
                                                                      } else {
                                                                           // Ktra 3 lần tung dice gần nhất
                                                                           let checkHavePrize = true;
                                                                           last3DiceHistory.forEach(diceHistory => {
                                                                                if (diceHistory.dice < 3) {
                                                                                     // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được prize
                                                                                     checkHavePrize = false;
                                                                                }
                                                                           });

                                                                           if (!checkHavePrize) {
                                                                                return res.status(200).json({
                                                                                     dice: dice,
                                                                                     prize: null,
                                                                                     voucher: findVoucher
                                                                                })
                                                                           } else {
                                                                                // Nếu đủ điều kiện nhận prize, tiến hành lấy random 1 prize trong prize Model
                                                                                prizeModel.count().exec((error, countPrize) => {
                                                                                     let randomPrize = Math.floor(Math.random * countPrize);
                                                                                     prizeModel.findOne().skip(randomPrize).exec((error, findPrize) => {
                                                                                          // Lưu vào prize History
                                                                                          let newPrizeHistory = {
                                                                                               _id: mongoose.Types.ObjectId(),
                                                                                               user: userExist._id,
                                                                                               prize: findPrize._id
                                                                                          }
                                                                                          prizeHistoryModel.create(newPrizeHistory, (error, prizeHistoryCreated) => {
                                                                                               if (error) {
                                                                                                    return res.status(500).json({
                                                                                                         message: error.message
                                                                                                    })
                                                                                               } else {
                                                                                                    // Trả về final result
                                                                                                    return res.status(200).json({
                                                                                                         dice: dice,
                                                                                                         prize: findPrize,
                                                                                                         voucher: findVoucher
                                                                                                    })
                                                                                               }
                                                                                          })
                                                                                     })
                                                                                })
                                                                           }
                                                                      }
                                                                 }
                                                            })
                                                  }
                                             })
                                        })
                                   })
                              }
                         }
                    })
               }
          }
     })
}

// Get dice history by username
const getDiceHistoryByUsername = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let username = req.query.username;
     // console.log(username)
     let condition = {};
     // B2: Validate dữ liệu
     if (!username) {
          return res.status(400).json({
               message: 'Username is required!'
          })
     } else {
          userModel.findOne({ username: username }, (error, userExist) => {
               if (error) {
                    return res.status(500).json({
                         status: "Error 500: Internal server error",
                         message: error.message
                    })
               } else {
                    if (!userExist) {
                         return res.status(200).json({
                              message: 'Username does not exist!',
                              diceHistory: []
                         })
                    } else {
                         condition.user = userExist._id
                         console.log(condition)
                         diceHistoryModel.find(condition)
                              .exec((error, data) => {
                                   if (error) {
                                        return res.status(500).json({
                                             status: "Error 500: Internal server error",
                                             message: error.message
                                        })
                                   }
                                   return res.status(200).json({
                                        message: `Get all dice histories of username ${username} successfully`,
                                        diceHistory: data
                                   })
                              })
                    }
               }
          })
     }
}

// Get prize history by username
const getPrizeHistoryByUsername = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let username = req.query.username;
     let condition = {};
     // B2: Validate dữ liệu
     if (!username) {
          return res.status(400).json({
               status: "Error 400: Bad Request",
               message: 'Username is required!'
          })
     } else {
          userModel.findOne({ username: username }, (error, userExist) => {
               if (error) {
                    return res.status(500).json({
                         status: "Error 500: Internal server error",
                         message: error.message
                    })
               } else {
                    if (!userExist) {
                         return res.status(500).json({
                              status: "Error 500: Internal server error",
                              message: 'Username does not exist!',
                              prizeHistory: []
                         })
                    } else {
                         condition.user = userExist._id
                         console.log(condition);
                         prizeHistoryModel.find(condition)
                              .exec((error, data) => {
                                   if (error) {
                                        return res.status(500).json({
                                             status: "Error 500: Internal server error",
                                             message: error.message
                                        })
                                   }
                                   return res.status(200).json({
                                        message: `Get all prize histories of username ${username} successfully`,
                                        prizeHistory: data
                                   })
                              })
                    }
               }
          })
     }
}

// Get voucher history by username
const getVoucherHistoryByUsername = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let username = req.query.username;
     let condition = {};
     // B2: Validate dữ liệu
     if (!username) {
          return res.status(400).json({
               status: "Error 400: Bad Request",
               message: 'Username is required!'
          })
     } else {
          userModel.findOne({ username: username }, (error, userExist) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               } else {
                    if (!userExist) {
                         return res.status(500).json({
                              status: "Error 500: Internal server error",
                              message: 'Username does not exist!',
                              voucherHistory: []
                         })
                    } else {
                         condition.user = userExist._id;
                         console.log(condition);
                         voucherHistoryModel.find(condition)
                              .exec((error, data) => {
                                   if (error) {
                                        return res.status(500).json({
                                             message: error.message
                                        })
                                   }
                                   return res.status(200).json({
                                        message: `Get all voucher histories of username ${username} successfully`,
                                        voucherHistory: data
                                   })
                              })
                    }
               }
          })
     }
}

module.exports = { diceController, getDiceHistoryByUsername, getPrizeHistoryByUsername, getVoucherHistoryByUsername };