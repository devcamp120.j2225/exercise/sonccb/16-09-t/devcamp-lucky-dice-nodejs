// Import thư viện express
const express = require('express');

// Tạo router
const router = express.Router();

// Import dice controller
const { diceController,
     getDiceHistoryByUsername,
     getPrizeHistoryByUsername,
     getVoucherHistoryByUsername } = require('../controllers/diceController');

// Create new dice
router.post('/devcamp-lucky-dice/dice', diceController);

// Get dice history by username
router.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUsername);

// Get prize history by username
router.get('/devcamp-lucky-dice/prize-history', getPrizeHistoryByUsername);

// Get voucher history by username
router.get('/devcamp-lucky-dice/voucher-history', getVoucherHistoryByUsername);

module.exports = router;
